#!/usr/bin/env node

const path = require('path');
const { spawn } = require('child_process');
const express = require('express');
const yargs = require('yargs');
const chalk = require('chalk');
const browser = require('browser-sync');

const VERSION = require('./package.json').version;


///////////////////////////////////////////////////////////////////////////////
// Options
///////////////////////////////////////////////////////////////////////////////
const options = yargs
  .option('port', {
    description: 'Port to serve on.',
    alias: 'p',
    type: 'number',
    default: 8080,
  })
  .options('sync', {
    description: 'Port to server browser-sync on.',
    alias: 's',
    type: 'number',
    default: 3000,
  })
  .option('directory', {
    description: 'Path to the directory to serve. Uses cwd if not given.',
    alias: 'd',
    type: 'string'
  })
  .options('spa', {
    description: 'Redirect all routes to index.html',
    type: 'boolean'
  })
  .options('nosync', {
    description: 'Disable browser-sync.',
    type: 'boolean'
  })
  .version(VERSION)
  .help()
  .alias('help', 'h')
  .argv
;

options.directory = options.directory || process.cwd();


///////////////////////////////////////////////////////////////////////////////
// Main
///////////////////////////////////////////////////////////////////////////////
const app = express();
app.use(express.static(options.directory));

if(options.spa) {
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(options.directory, 'index.html'));
  });
}


app.listen(options.port, () => {
  console.log(chalk.bold.cyan('quickserve!'), chalk.dim(` v${VERSION}`));

  console.log(
    logKey('Serving'),
    chalk.bold(options.directory),
    options.spa ? `as ${chalk.bold('SPA')}` : ''
  );

  console.log(logKey('Server URL'), chalk.bold(`http://localhost:${options.port}`));

  if(!options.nosync) {
    spawn(`npx browser-sync start --proxy=http://localhost:${options.port} --port ${options.sync} --files "${options.directory}/"`, {
      shell: true,
      stdio: 'ignore',
      cwd: __dirname,
    });
    console.log(logKey('Sync URL'), chalk.bold(`http://localhost:${options.sync}`));
  }
  console.log();
  console.log('Press', chalk.bold('Ctrl+C'), 'to close.');
})
.on('close', () => {
  console.log('Bye!');
});


process.on('SIGINT', function() {
  console.log("\nBye!");
  process.exit();
});


///////////////////////////////////////////////////////////////////////////////
// Util
///////////////////////////////////////////////////////////////////////////////
function logKey(key) {
  return key.padStart(10);
}
