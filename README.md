**quickserve!**
===========

### Quick command-line development server. ###

## Features
 - Serve any local directory.
 - Serve single page applications (SPAs); React, Angular, etc.
 - Browser-sync
----
## Useage
### To serve the current directory:
```bash
quickserve
```

### To serve another directory:
```bash
quickserve --directory /path/to/my website
# or
quickserve --d /path/to/my website
```

### To serve a SPA:
```bash
quickserve --spa
```
All routes will be directed to index.html, allowing the app to handle routing.


### To set the port to serve on:
```bash
quickserve --port 9999
# or
quickserve -p 9999
```

### To set the port to run browser-sync on:
```bash
quickserve --sync 8888
# or
quickserve -s 8888
```

### To disable browser-sync:
```bash
quickserve --nosync
```
